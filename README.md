# synth-data
A test repository for generating differentially-private synthetic data using the smartnoise synth package.

## Installation
1. Clone this repo and run `cd synth-data`
2. Create a virtual environment (conda or python)
3. Run `pip install -r requirements.txt`
4. Open a jupyter notebook (can be done by running `jupyter notebook`)
5. Experiment with various data synthesizers

## Notes
The computational load that smartnoise synthesizers (particularly the ML-based ones) put on your computer is proportional to the size and dimensionality of the underlying data.

This is implemented in various places throughout the notebook, but doing the following will improve efficiency:
1. removing columns with only one value
2. ensuring that JSON values have been exploded into individual rows
3. specifying continuous, ordinal, and categorical columns
4. limiting the number of source rows

As a guidepost, I was able to consistenly have CSVs with ~2,000 rows function perfectly on stat1006. Upping the input data to ~32,000 rows crashed the machine.

Another note: non-ML methods (MST, MWEM, AIM) will often produce very similar values for many generated rows. ML methods will produce more continuous values.

## Authors and acknowledgment
Made by Hal Triedman (htriedman@wikimedia.org)

## License
This project is licensed under CC0.
